namespace java com.tiantian.sng.thrift.task

const string version = '1.0.0'

service SngTaskService {
  string getServiceVersion(),
  bool execTask(1:string taskStr)
}
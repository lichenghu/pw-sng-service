namespace java com.tiantian.sng.thrift.event

const string version = '1.0.1'

struct SngEventResponse {
  1:i32 status,
  2:string message
  3:string maxUsers
  4:i64 waitSec
}


struct SngRoom {
     1:string roomId
     2:string roomName
     3:string roomDesc
     4:i64 fee
     5:i32 usersCnt
     6:list<SngReward> rewards
     7:list<SngRule> rules
}

struct SngReward {
    1:i32 ranking
    2:string physicalReward
    3:i64 score
}

struct SngRule {
    1:i32 level
    2:i64 smallBlind
    3:i64 bigBlind
    4:i64 upBlindSecs
}

struct UserStatus {
 1:i32 sn,
 2:string status
}

struct UserBet {
 1:i32 sn,
 2:i64 bet
}

struct UserInfo {
  1:string userId,
  2:i32 sitNum,
  3:string  avatarUrl,
  4:string  nickName,
  5:i64  money,
  6:i32  playing,
  7:string gender
}

struct TableInfo {
    1:list<UserInfo>    users,
    2:list<UserStatus>  userStatus,
    3:string            deskCards,
    4:list<UserBet>     userBets,
    5:string            innerId,
    6:i32               curBetSit,
    7:string            handCards,
    8:string            pool,
    9:i64               leftSecs,
    10:string           innerCnt,
    11:string           userCanOps,
    12:i32              button,
    13:i32              smallBtn,
    14:i32              bigBtn,
    15:i32              smallBtnMoney,
    16:i32              bigBtnMoney,
    17:i32              showBegin,
    18:i32              isStopped,
    19:string           gameStatus,
    20:string           pwd,
    21:string           cardLevel,
    22:string           tableId,
    23:string           roomId,
    24:i64              bonus,
    25:i64              totalSecs,
    26:i32              waitNums,
    27:string           ranking
}

struct UserRanking {
    1:i32 ranking
    2:string name
    3:string avatarUrl
    4:i64 chips
    5:string userId
}

struct TableDetail {
    1:list<UserRanking> rankings
    2:list<SngRule> rules
    3:list<SngReward> rewards
    4:i32 currentRuleLevel
}

service SngEventService {
    string getServiceVersion(),
    void sngGameEvent(1:string cmd, 2:string userId, 3:string  data, 4:string tableId),
    SngEventResponse join(1:string userId, 2:string roomId),
    list<SngRoom> getClubRoomList(1:string clubId),
    TableInfo getTableInfo(1:string tableId, 2:string userId),
    bool exit(1:string tableId, 2:string userId),
    map<string, string> checkJoin(1:string userId, 2:string tableId),
    bool rejoin(1:string userId, 2:string tableId),
    TableDetail getTableDetail(1:string tableId)
    string checkUserCurrentTable(1:string userId)
    map<string, string> checkUserCurrentTableWithUserCnt(1:string userId)
    void closeSngRoom(1:string roomId, 2: map<string, list<string>> tableUserIds)
    string getGameRecord(1:string tableId, 2:string userId, 3:i32 cnt)
}
package com.tiantian.framework.thrift.client;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

/**
 *
 */
public class ClientFactory extends BasePooledObjectFactory<TProtocol> {
    private String host;
    private int port;
    public ClientFactory(String host, int port) {
        this.host = host;
        this.port = port;
    }

    @Override
    public TProtocol create() throws Exception {
        TTransport transport = createSocket();
        TTransport tTransport = new TFastFramedTransport(transport);
        return new TCompactProtocol(tTransport);
    }

    public TSocket createSocket() throws Exception {
        TSocket transport = new TSocket(host, port);
        transport.open();
        return transport;
    }

    @Override
    public PooledObject<TProtocol> wrap(TProtocol obj) {
        return new DefaultPooledObject<>(obj);
    }
}

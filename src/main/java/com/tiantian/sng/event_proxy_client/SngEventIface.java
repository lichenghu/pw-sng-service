package com.tiantian.sng.event_proxy_client;

import com.tiantian.sng.thrift.event.SngEventService;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TSocket;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;

/**
 *
 */
public class SngEventIface {
    private static SngEventService.Iface clientProxy;

    private static class SngEventIfaceHolder {
        private final static SngEventIface instance = new SngEventIface();
    }
    private SngEventIface() {
        ClassLoader classLoader = SngEventService.Iface.class.getClassLoader();
        clientProxy = (SngEventService.Iface) Proxy.newProxyInstance(classLoader,
                new Class[]{SngEventService.Iface.class},
                (proxy, method, args) -> {
                    TSocket ttSocket = ClientPool.getInstance().borrowObject();
                    try {
                        TFastFramedTransport fastTransport = new TFastFramedTransport(ttSocket);
                        TCompactProtocol protocol = new TCompactProtocol(fastTransport);
                        SngEventService.Client client = new SngEventService.Client(protocol);
                        //设置成可以访问
                        method.setAccessible(true);
                        return method.invoke(client, args);
                    } catch (InvocationTargetException | IllegalAccessException | IllegalArgumentException e) {
                        if (e instanceof InvocationTargetException) {
                            Throwable throwable = ((InvocationTargetException) e).getTargetException();
                            throwable.printStackTrace();
                            throw throwable;
                        }
                        e.printStackTrace();
                        throw e;
                    } finally {
                        ClientPool.getInstance().returnObject(ttSocket);
                    }
                });
    }

    public static SngEventIface instance() {
        return SngEventIfaceHolder.instance;
    }

    public SngEventService.Iface iface() {
        return clientProxy;
    }
}

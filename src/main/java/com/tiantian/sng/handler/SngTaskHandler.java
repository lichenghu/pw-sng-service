package com.tiantian.sng.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.sng.akka.ClusterClientManager;
import com.tiantian.sng.akka.event.TableTaskEvent;
import com.tiantian.sng.consts.GameConstants;
import com.tiantian.sng.thrift.task.SngTaskService;
import com.tiantian.sng.thrift.task.sng_taskConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;

/**
 *
 */
public class SngTaskHandler implements SngTaskService.Iface {
    @Override
    public String getServiceVersion() throws TException {
        return sng_taskConstants.version;
    }

    @Override
    public boolean execTask(String taskStr) throws TException {
        try {
            JSONObject jsonObject = JSON.parseObject(taskStr);
            String taskEvent = jsonObject.getString(GameConstants.TASK_EVENT);
            if (StringUtils.isNotBlank(taskEvent)) {
                TableTaskEvent event = new TableTaskEvent();
                event.setEvent(taskEvent);
                event.setParams(jsonObject);
                ClusterClientManager.send(event);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}

package com.tiantian.sng.handler;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.tiantian.club.proxy_client.ClubIface;
import com.tiantian.core.proxy_client.AccountIface;
import com.tiantian.core.thrift.account.UserDetail;
import com.tiantian.sng.akka.ClusterClientManager;
import com.tiantian.sng.akka.event.user.*;
import com.tiantian.sng.akka.result.TableInfoResult;
import com.tiantian.sng.akka.result.UserBetResult;
import com.tiantian.sng.akka.result.UserInfoResult;
import com.tiantian.sng.akka.result.UserStatusResult;
import com.tiantian.sng.consts.RedisConts;
import com.tiantian.sng.data.mongodb.MGDatabase;
import com.tiantian.sng.data.redis.RedisUtil;
import com.tiantian.sng.entity.SngRewardEntity;
import com.tiantian.sng.entity.SngRoomEntity;
import com.tiantian.sng.entity.SngRuleEntity;
import com.tiantian.sng.model.GameRecord;
import com.tiantian.sng.thrift.event.*;
import com.tiantian.sng.utils.RecordUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.bpsm.edn.printer.Printers;

import java.util.*;

/**
 *
 */
public class SngEventHandler implements SngEventService.Iface{
    static Logger LOG = LoggerFactory.getLogger(SngEventHandler.class);
    private static final String SNG_ROOM  = "sng_rooms";
    private final static String SNG_TABLES = "sng_tables";
    private static final String SPINGO_USER_WAIT_ROOM_KEY = "sng_wait_room:";
    private static final String USER_SPINGO_TABLE_KEY = "user_sng_table_key:";
    private static final String SPINGO_TABLE_STATUS_KEY = "sng_table_status:";
    private static final String USER_SPINGO_GAME_KEY = "user_sng_games_key:";
    private static final String SPINGO_TABLE_USER_KEY = "sng_table_user_key:";
    private static final String SPINGO_ROOM_ONLINE_USERS_KEY = "sng_room_online_users_status:";
    private static long WAIT_SECS = 25;
    @Override
    public String getServiceVersion() throws TException {
        return sng_eventConstants.version;
    }

    @Override
    public void sngGameEvent(String cmd, String userId, String data, String tableId) throws TException {
        switch (cmd) {
            case "fold" :
                TableUserFoldEvent tableUserFoldEvent = new TableUserFoldEvent(tableId, userId, data);
                ClusterClientManager.send(tableUserFoldEvent);
                break;
            case "check" :
                TableUserCheckEvent tableUserCheckEvent = new TableUserCheckEvent(tableId, userId, data);
                ClusterClientManager.send(tableUserCheckEvent);
                break;
            case "call" :
                TableUserCallEvent tableUserCallEvent = new TableUserCallEvent(tableId, userId, data);
                ClusterClientManager.send(tableUserCallEvent);
                break;
            case "raise" :
                try {
                    String[] datas = data.split(",");
                    long raise = Long.parseLong(datas[0]);
                    String pwd = datas[1];
                    TableUserRaiseEvent tableUserRaiseEvent = new TableUserRaiseEvent(tableId, userId, raise, pwd);
                    ClusterClientManager.send(tableUserRaiseEvent);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
                break;
            case "allin" :
                TableUserAllinEvent tableUserAllinEvent = new TableUserAllinEvent(tableId, userId, data);
                ClusterClientManager.send(tableUserAllinEvent);
                break;
            case "fast_raise" :
                try{
                    String[] datas = data.split(",");
                    TableUserFastRaiseEvent tableUserFastRaiseEvent = new TableUserFastRaiseEvent(tableId, userId, datas[0],  datas[1]);
                    ClusterClientManager.send(tableUserFastRaiseEvent);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
                break;
            case "show_cards" :
                TableUserShowCardsEvent tableUserShowCardsEvent = new TableUserShowCardsEvent(tableId, userId, data);
                ClusterClientManager.send(tableUserShowCardsEvent);
                break;
            case "sync_inf" :
                rejoin(userId, tableId);
                break;
            case "emoji" :
                try {
                    String[] datas = data.split(",");
                    String toUserId = datas[0];
                    String emoji = datas[1];
                    TableUserEmojiEvent tableUserEmojiEvent = new TableUserEmojiEvent(tableId, userId, toUserId, emoji);
                    ClusterClientManager.send(tableUserEmojiEvent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }

    public List<SngRoom> getClubRoomList(String clubId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(SNG_ROOM);
        BasicDBObject selectCondition = new BasicDBObject();
        selectCondition.put("clubId", clubId);
        selectCondition.put("isEnd",  new BasicDBObject("$eq", 0)); // 1是已结束, 0是未结束
        FindIterable<Document> limitIter = collection.find(selectCondition)
                .sort(new BasicDBObject("createDate", -1));
        MongoCursor<Document> cursor = limitIter.iterator();
        List<SngRoom> sngRooms = Lists.newArrayList();
        while (cursor.hasNext()) {
               Document doc = cursor.next();
               SngRoomEntity sngRoomEntity = getFromDoc(doc);
               if (sngRoomEntity != null) {
                   SngRoom sngRoom = transformRoom(sngRoomEntity);
                   sngRooms.add(sngRoom);
               }
        }
        return sngRooms;
    }

    //报名
    public SngEventResponse join(String userId, String newRoomId) {
        UserDetail userDetail = null;
        try {
             userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
        } catch (TException e) {
            e.printStackTrace();
        }
        if (userDetail == null) {
            return new SngEventResponse(-1, "用户信息错误", "0", 0);
        }
       // 查找房间里面的桌子未满
       SngRoomEntity sngRoomEntity = getSngRoom(newRoomId);
       if (sngRoomEntity == null) {
           return new SngEventResponse(-1, "房间信息不存在", "0", 0);
       }
       if (sngRoomEntity.getIsEnd() == 1) {
           return new SngEventResponse(-2, "房间已关闭", "0", 0);
       }
       RoomUserJoinEvent.Response response =
               (RoomUserJoinEvent.Response) ClusterClientManager.sendRoomEventAndWait(new RoomUserJoinEvent(newRoomId,
                       sngRoomEntity.getClubId(), userId, userDetail.getAvatarUrl(), userDetail.getNickName(),
                       sngRoomEntity.getMaxTableNums(), sngRoomEntity.getUserCnt(),
                       sngRoomEntity.getFee()));
       if (response == null) {
           return new SngEventResponse(-3, "加入房间失败", "0", 0);
       }

       if (response.getStatus() == -1) { // 房间已经满了
           return new SngEventResponse(-4, "房间人数已满", "0", 0);
       }
        if (response.getStatus() == -2) { // 积分不足
            return new SngEventResponse(-5, "积分不足", "0", 0);
        }
       String tableId = response.getTableId();
       int sitNum = response.getSitNum();
       int tableIndex = response.getTableIndex();
       List<TableUserJoinEvent.Rule> rules = Lists.newArrayList();
       for (SngRuleEntity sngRuleEntity : sngRoomEntity.getRules())  {
            rules.add(new TableUserJoinEvent.Rule(sngRuleEntity.getLevel(), sngRuleEntity.getSmallBlind(),
                    sngRuleEntity.getBigBlind(), sngRuleEntity.getUpBlindSecs()));
       }
       List<TableUserJoinEvent.Reward> rewards = Lists.newArrayList();
       for (SngRewardEntity sngRewardEntity : sngRoomEntity.getSngRewards()) {
            rewards.add(new TableUserJoinEvent.Reward(sngRewardEntity.getRanking(), sngRewardEntity.getScore(),
                    sngRewardEntity.getAwardId(), sngRewardEntity.getPhysicalReward()));
       }
       TableUserJoinEvent tableUserJoinEvent = new TableUserJoinEvent(newRoomId, sngRoomEntity.getRoomName(), userId, tableId, sitNum,
               tableIndex,  userDetail.getAvatarUrl(),  userDetail.getNickName(), userDetail.getGender(),sngRoomEntity.getBuyIn(),
               sngRoomEntity.getUserCnt(), rules, rewards);
       String result = (String) ClusterClientManager.sendAndWait(tableUserJoinEvent, 20);
       if("ok".equalsIgnoreCase(result)) {
          return new SngEventResponse(0, tableId, sngRoomEntity.getUserCnt() + "", WAIT_SECS);
       }
       return new SngEventResponse(-3, "加入房间失败", "0", 0);
    }


    // 退出房间, 自动挂机或者取消报名
    public boolean exit(String tableId, String userId) {
        LOG.info("tableId:" + tableId  +",userId:"+userId);
        if (StringUtils.isBlank(tableId) || StringUtils.isBlank(userId)) {
            LOG.error("exit ERROR tableId is :" +tableId+";userId:" + userId);
            return true;
        }
        boolean isGameOver = checkTableInfoIsOver(tableId);
        if (isGameOver) {
            LOG.info("game over tableId is :" + tableId + ";userId:" + userId);
            return true;
        }
        String result = (String) ClusterClientManager.sendAndWait(new TableUserExitEvent(tableId, userId, false));
        Map<String, String> map = getSngTableInfo(tableId);
        if (map == null || map.isEmpty()) {
            LOG.info("SngTableInfo is null tableId is :" +tableId+";userId:" + userId);
            return true;
        }
        String clubId = map.get("clubId");
        String roomId = map.get("roomId");
        LOG.info("result is :" + result);
        if ("true_exit".equals(result)) { // 退出需要归还积分 游戏未开始
            SngRoomEntity sngRoomEntity = getSngRoom(roomId);
            long score = 0;
            if (sngRoomEntity != null) {
                // 归还积分
                score = sngRoomEntity.getFee();
            }
            String ret = (String) ClusterClientManager.sendRoomEventAndWait(new RoomUserExitEvent(roomId, userId, tableId)); // 退出
            try {
                if ("1".equals(ret)) {
                    ClubIface.instance().iface().addUserClubScore(userId, clubId, score);
                }
            } catch (Exception e) {
                e.printStackTrace();
                LOG.error("增加积分失败:userId" + userId +"; clubId:" + clubId + ";score:" + score);
            }
        }
        else { // 游戏已经开始 退出更新状态
            ClusterClientManager.sendRoomEvent(new RoomUserExitUpdateStatusEvent(roomId, userId, tableId));
        }
        return true;
    }


    public Map<String, String> checkJoin(String userId) { // 判断玩家是否在房间里面
        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("roomId", "");
        resultMap.put("tableId", "");
        Set<String> keys = RedisUtil.getKeys(USER_SPINGO_TABLE_KEY + userId + "*");
        for (String key : keys) {
             Map<String, String> tableUserMap = RedisUtil.getMap(key);
             if (tableUserMap != null) {
                 String status = tableUserMap.get("status");
                 String roomId = tableUserMap.get("roomId");
                 String tableId = tableUserMap.get("tableId");
                 if ("gaming".equalsIgnoreCase(status)) {
                     resultMap.put("roomId", roomId);
                     resultMap.put("tableId",tableId);
                     return resultMap;
                 }
             }
        }
        return resultMap;
    }

    public Map<String, String> checkJoin(String userId, String tableId) { // 判断玩家是否在房间里面
        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("status", "0"); // 在房间
        resultMap.put("errMsg", "");
        boolean isOver = checkTableUserIsOver(tableId, userId);
        if (isOver) {
            resultMap.put("status", "-1");
            resultMap.put("errMsg", "已不在局中");
            return resultMap;
        }
        Map<String, String> userMap = RedisUtil.getMap(USER_SPINGO_TABLE_KEY + userId + ":" + tableId);
        if (userMap == null) {
            resultMap.put("status", "-2");
            resultMap.put("errMsg", "已不在局中");
            return resultMap;
        }
        return resultMap;
    }

    private boolean checkTableUserIsOver(String tableId, String userId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(SNG_TABLES);
        BasicDBObject selectCondition = new BasicDBObject();
        selectCondition.put("_id", new ObjectId(tableId));
        FindIterable<Document> limitIter = collection.find(selectCondition);
        MongoCursor<Document> cursor = limitIter.iterator();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            List<Document> tableUsers = doc.get("tableUsers", List.class);
            if (tableUsers != null) {
                for (Document document : tableUsers) {
                     String uId = document.getString("userId");
                     if (uId.equalsIgnoreCase(userId)) {
                         String status = document.getString("status");
                         return status.equals("over");
                     }
                }
            }
        }
        return false;
    }

    public boolean rejoin(String userId, String tableId) {
        ClusterClientManager.send(new TableUserRejoinEvent(tableId, userId));
        return true;
    }

    public TableInfo getTableInfo(String tableId, String userId) { // tableId2废弃
        Map<String, String> joinRet = getSngTableInfo(tableId);
        String roomId = joinRet.get("roomId");
        String isEnd = joinRet.get("isEnd");
        if (StringUtils.isBlank(tableId) || "1".equalsIgnoreCase(isEnd)) {
            TableInfo tableInfo = new TableInfo();
            tableInfo.setGameStatus("over");
            return tableInfo;
        }

        TableInfo gTableInfo = new TableInfo();
        TableInfoResult tableInfo =
                (TableInfoResult)ClusterClientManager.sendAndWait(new TableUserTableInfEvent(tableId, userId), 5);
        if (tableInfo != null) {
            gTableInfo.setRoomId(roomId);
            gTableInfo.setTableId(tableId);
            gTableInfo.setInnerId(tableInfo.getInnerId());
            gTableInfo.setInnerCnt(tableInfo.getInnerCnt());
            List<UserInfo> userInfos = new ArrayList<>();
            if (tableInfo.getUsers() != null) {
                for (UserInfoResult userInfo : tableInfo.getUsers()) {
                    UserInfo $userInfo = new UserInfo();
                    $userInfo.setUserId(userInfo.getUserId());
                    $userInfo.setSitNum(userInfo.getSitNum());
                    $userInfo.setNickName(userInfo.getNickName());
                    $userInfo.setAvatarUrl(userInfo.getAvatarUrl());
                    $userInfo.setMoney(userInfo.getMoney());
                    $userInfo.setPlaying(userInfo.getPlaying());
                    $userInfo.setGender(userInfo.getGender());
                    userInfos.add($userInfo);
                }
            }
            gTableInfo.setUsers(userInfos);
            List<UserStatus> userStatuses = new ArrayList<>();
            if (tableInfo.getUserStatus() != null) {
                for (UserStatusResult userStatus : tableInfo.getUserStatus()) {
                    UserStatus $userStatus = new UserStatus();
                    $userStatus.setSn(userStatus.getSn());
                    $userStatus.setStatus(userStatus.getStatus());
                    userStatuses.add($userStatus);
                }
            }
            gTableInfo.setUserStatus(userStatuses);
            gTableInfo.setCurBetSit(tableInfo.getCurBetSit());
            gTableInfo.setDeskCards(tableInfo.getDeskCards());
            gTableInfo.setHandCards(tableInfo.getHandCards());
            gTableInfo.setCardLevel(tableInfo.getCardLevel());
            gTableInfo.setLeftSecs(tableInfo.getLeftSecs());
            gTableInfo.setPool(tableInfo.getPool());
            List<UserBet> userBets = new ArrayList<>();
            if (tableInfo.getUserBets() != null) {
                for (UserBetResult userBet : tableInfo.getUserBets()) {
                    UserBet $userBets = new UserBet();
                    $userBets.setSn(userBet.getSn());
                    $userBets.setBet(userBet.getBet());
                    userBets.add($userBets);
                }
            }
            gTableInfo.setUserBets(userBets);
            gTableInfo.setPwd(tableInfo.getPwd());
            gTableInfo.setUserCanOps(tableInfo.getUserCanOps());
            gTableInfo.setButton(tableInfo.getButton());
            gTableInfo.setSmallBtn(tableInfo.getSmallBtn());
            gTableInfo.setBigBtn(tableInfo.getBigBtn());
            gTableInfo.setSmallBtnMoney(tableInfo.getSmallBtnMoney());
            gTableInfo.setBigBtnMoney(tableInfo.getBigBtnMoney());
            gTableInfo.setShowBegin(tableInfo.getShowBegin());
            gTableInfo.setIsStopped(tableInfo.getIsStopped());
            gTableInfo.setGameStatus(tableInfo.getGameStatus());
            gTableInfo.setTotalSecs(tableInfo.getTotalSecs());
            gTableInfo.setWaitNums(tableInfo.getWaitNums());
            gTableInfo.setRanking(tableInfo.getRanking());
        }
        else {
             gTableInfo.setGameStatus("over"); // 游戏已经结束
        }
        return gTableInfo;
    }

    public String checkUserCurrentTable(String userId) {
        Set<String> keys = RedisUtil.getKeys(USER_SPINGO_TABLE_KEY + userId + "*");
        for (String key : keys) {
             Map<String, String> tableUserMap = RedisUtil.getMap(key);
             if (tableUserMap != null) {
                String status = tableUserMap.get("status");
                String tableId = tableUserMap.get("tableId");
                if ("gaming".equalsIgnoreCase(status) ||
                        "standing".equalsIgnoreCase(status) ||
                            "preparing".equalsIgnoreCase(status)) {
                    rejoin(userId, tableId);
                    return tableId;
                }
            }
        }
        return "";
    }

    public Map<String, String> checkUserCurrentTableWithUserCnt(String userId) {
        Map<String, String> map = new HashMap<String, String>(){{
            put("tableId", "");
            put("maxUsers", "0");
            put("waitSecs", WAIT_SECS + "");
        }};
        Set<String> keys = RedisUtil.getKeys(USER_SPINGO_TABLE_KEY + userId + "*");
        for (String key : keys) {
            Map<String, String> tableUserMap = RedisUtil.getMap(key);
            if (tableUserMap != null) {
                String status = tableUserMap.get("status");
                String tableId = tableUserMap.get("tableId");
                if ("gaming".equalsIgnoreCase(status) ||
                        "standing".equalsIgnoreCase(status) ||
                        "preparing".equalsIgnoreCase(status)) {
                    rejoin(userId, tableId);
                    map.put("tableId", tableId);
                    String key2 = RedisConts.SPINGO_TABLE_STATUS_KEY + tableId;
                    Map<String, String> tableStatusMap = RedisUtil.getMap(key2);
                    String maxUsers = tableStatusMap.get("maxUsers");
                    if (maxUsers != null) {
                        map.put("maxUsers", maxUsers);
                    }
                    map.put("waitSecs", WAIT_SECS + "");
                    return map;
                }
            }
        }
        return map;
    }

    public void clearDeadTableUsers() {
        Set<String> keys = RedisUtil.getKeys(USER_SPINGO_TABLE_KEY + "*");
        for (String key : keys) {
             Map<String, String> tableUserMap = RedisUtil.getMap(key);
             if (tableUserMap != null) {
                 String status = tableUserMap.get("status");
                 String roomId = tableUserMap.get("roomId");
                 String tableId = tableUserMap.get("tableId");
                 RedisUtil.del(SPINGO_TABLE_STATUS_KEY + tableId);
                 String userId = key.replace(USER_SPINGO_TABLE_KEY, "").replace(":"+tableId, "").trim();
                 RedisUtil.del(USER_SPINGO_GAME_KEY + userId);
                 RedisUtil.del(SPINGO_TABLE_USER_KEY + tableId);
                 RedisUtil.srem(SPINGO_ROOM_ONLINE_USERS_KEY + roomId, userId);
             }
             RedisUtil.del(key);
        }
    }

    private SngRoomEntity getSngRoom(String roomId) {
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(SNG_ROOM);
        FindIterable<Document> limitIter = groupsCollection.find(new BasicDBObject("_id", new ObjectId(roomId)))
                .sort(new BasicDBObject()).limit(1);
        MongoCursor<Document> cursor = limitIter.iterator();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            return getFromDoc(doc);
        }
        return null;
    }

    private SngRoomEntity getFromDoc(Document doc) {
        if (doc == null) {
            return null;
        }
        ObjectId objectId = doc.getObjectId("_id");
        String clubId = doc.getString("clubId");
        String roomName = doc.getString("roomName");
        String roomDesc = doc.getString("roomDesc");
        Integer isEnd = doc.getInteger("isEnd");
        Long fee = doc.getLong("fee");
        Integer userCnt = doc.getInteger("userCnt");
        Integer maxTableNums = doc.getInteger("maxTableNums");
        Long buyIn = doc.getLong("buyIn");
        List<Document> rules = doc.get("rules", List.class);
        List<SngRuleEntity> ruleEntities = new ArrayList<>();
        if (rules != null) {
            for (Document document : rules) {
                 Integer level = document.getInteger("level");
                 Long smallBlind = document.getLong("smallBlind");
                 Long bigBlind = document.getLong("bigBlind");
                 Long upBlindSecs = document.getLong("upBlindSecs");
                 SngRuleEntity sngRuleEntity = new SngRuleEntity(level, smallBlind, bigBlind, upBlindSecs);
                 ruleEntities.add(sngRuleEntity);
            }
        }
        List<Document> sngRewards = doc.get("sngRewards", List.class);
        List<SngRewardEntity> sngRewardEntities = new ArrayList<>();
        if (sngRewards != null) {
            for (Document document : sngRewards) {
                Integer ranking = document.getInteger("ranking");
                String physicalReward = document.getString("physicalReward");
                String awardId = document.getString("awardId");
                Long score = document.getLong("score");
                SngRewardEntity sngRewardEntity = new SngRewardEntity();
                sngRewardEntity.setRanking(ranking);
                sngRewardEntity.setPhysicalReward(physicalReward);
                sngRewardEntity.setScore(score);
                sngRewardEntity.setAwardId(awardId);
                sngRewardEntities.add(sngRewardEntity);
            }
        }
        SngRoomEntity sngRoomEntity = new SngRoomEntity();
        sngRoomEntity.setRoomId(objectId.toString());
        sngRoomEntity.setRoomName(roomName);
        sngRoomEntity.setRoomDesc(roomDesc);
        sngRoomEntity.setIsEnd(isEnd);
        sngRoomEntity.setFee(fee);
        sngRoomEntity.setUserCnt(userCnt);
        sngRoomEntity.setMaxTableNums(maxTableNums);
        sngRoomEntity.setClubId(clubId);
        sngRoomEntity.setRules(ruleEntities);
        sngRoomEntity.setSngRewards(sngRewardEntities);
        sngRoomEntity.setBuyIn(buyIn);
        return sngRoomEntity;
    }

    private Map<String, String> getSngTableInfo(String tableId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(SNG_TABLES);
        BasicDBObject selectCondition = new BasicDBObject();
        selectCondition.put("_id", new ObjectId(tableId));
        FindIterable<Document> limitIter = collection.find(selectCondition);
        MongoCursor<Document> cursor = limitIter.iterator();
        Map<String, String> map = Maps.newHashMap();
        while (cursor.hasNext()) {
               Document doc = cursor.next();
               map.put("roomId",  doc.getString("roomId"));
               map.put("clubId",  doc.getString("clubId"));
               map.put("isEnd", doc.getInteger("isEnd").toString());
        }
        return map;
    }

    private List<UserRanking> getSngTableRankingInfos(String tableId, long totalBuyIn) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(SNG_TABLES);
        BasicDBObject selectCondition = new BasicDBObject();
        selectCondition.put("_id", new ObjectId(tableId));
        FindIterable<Document> limitIter = collection.find(selectCondition);
        MongoCursor<Document> cursor = limitIter.iterator();
        List<UserRanking> userRankings = Lists.newArrayList();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            List<Document> tableUsers = doc.get("tableUsers", List.class);
            if (tableUsers != null) {
                for (Document document : tableUsers) {
                     UserRanking userRanking = new UserRanking();
                     String userId = document.getString("userId");
                     String nickName = document.getString("nickName");
                     Integer ranking = document.getInteger("ranking");
                     String avatarUrl = document.getString("avatarUrl");
                     userRanking.setUserId(userId);
                     userRanking.setName(nickName);
                     userRanking.setRanking(ranking);
                     userRanking.setAvatarUrl(avatarUrl);
                     long chips = getUserChips(userId, tableId);
                     if (ranking ==1 && chips == 0) {
                         chips = totalBuyIn;
                     }
                     userRanking.setChips(chips);
                     userRankings.add(userRanking);
                }
            }
        }
        if(userRankings.size() > 0) {
            Collections.sort(userRankings, (o1, o2) -> o1.getRanking() - o2.getRanking());
        }
        return userRankings;
    }


    public TableDetail getTableDetail(String tableId) {
        Map<String, String> tbInfos = getSngTableInfo(tableId);
        String roomId = tbInfos.get("roomId");
        String clubId = tbInfos.get("clubId");
        if (StringUtils.isBlank(roomId) || StringUtils.isBlank(clubId)) {
            return new TableDetail();
        }
        SngRoomEntity sngRoomEntity = getSngRoom(roomId);
        if (sngRoomEntity == null) {
            return new TableDetail();
        }
        SngRoom sngRoom = transformRoom(sngRoomEntity);
        TableDetail tableDetail = new TableDetail();
        tableDetail.setRewards(sngRoom.getRewards());
        tableDetail.setRules(sngRoom.getRules());
        List<UserRanking> userRankings = getSngTableRankingInfos(tableId, sngRoomEntity.getBuyIn() * sngRoomEntity.getUserCnt());
        tableDetail.setRankings(userRankings);
        String key = RedisConts.SPINGO_TABLE_STATUS_KEY + tableId;
        Map<String, String> tableUserMap = RedisUtil.getMap(key);
        String currentBlindLevel = tableUserMap.get("currentBlindLevel");
        int lvl = 0;
        if (StringUtils.isNotBlank(currentBlindLevel)) {
            lvl = Integer.parseInt(currentBlindLevel);
        }
        tableDetail.setCurrentRuleLevel(lvl);
        return tableDetail;
    }

    public void closeSngRoom(String roomId, Map<String, List<String>> tableUserIds) {
        if (tableUserIds != null) {
            for (Map.Entry<String, List<String>> entry : tableUserIds.entrySet()) {
                 String tableId = entry.getKey();
                 List<String> userIds = entry.getValue();
                 if (userIds != null && userIds.size() > 0) {
                    for (String uId : userIds) {
                         try {
                             exit(tableId, uId);
                         }
                         catch (Exception e){
                             e.printStackTrace();
                         }
                    }
                 }
            }
        }
    }

    @Override
    public String getGameRecord(String tableId, String userId, int cnt) throws TException {
        if (tableId != null) {
            if (cnt <= 0) {
                cnt = 1;
            }
            GameRecord gameRecord = RecordUtils.getRecordByIndex(tableId, cnt);
            if (gameRecord == null) {
                return "";
            }
            gameRecord.generateProgresses();
            return Printers.printString(Printers.prettyPrinterProtocol(), gameRecord.getRecord());
        }
        return "";
    }

    private SngRoom transformRoom(SngRoomEntity sngRoomEntity) {
        SngRoom sngRoom = new SngRoom();
        sngRoom.setRoomId(sngRoomEntity.getRoomId());
        sngRoom.setRoomName(sngRoomEntity.getRoomName());
        sngRoom.setRoomDesc(sngRoomEntity.getRoomDesc());
        sngRoom.setFee(sngRoomEntity.getFee());
        sngRoom.setUsersCnt(sngRoomEntity.getUserCnt());
        List<SngRule> sngRules = Lists.newArrayList();
        for (SngRuleEntity sngRuleEntity : sngRoomEntity.getRules()) {
            SngRule sngRule = new SngRule();
            sngRule.setLevel(sngRuleEntity.getLevel());
            sngRule.setBigBlind(sngRuleEntity.getBigBlind());
            sngRule.setSmallBlind(sngRuleEntity.getSmallBlind());
            sngRule.setUpBlindSecs(sngRuleEntity.getUpBlindSecs());
            sngRules.add(sngRule);
        }
        sngRoom.setRules(sngRules);
        List<SngReward> sngRewards = Lists.newArrayList();
        for (SngRewardEntity sngRewardEntity : sngRoomEntity.getSngRewards()) {
            SngReward sngReward = new SngReward();
            sngReward.setRanking(sngRewardEntity.getRanking());
            sngReward.setScore(sngRewardEntity.getScore());
            sngReward.setPhysicalReward(sngRewardEntity.getPhysicalReward());
            sngRewards.add(sngReward);
        }
        sngRoom.setRewards(sngRewards);
        return sngRoom;
    }

    private long getUserChips(String userId, String tableId) {
        String userChips = RedisUtil.getFromMap(RedisConts.USER_SPINGO_GAMES_KEY + userId, tableId);
        JSONObject jsonObject = null;
        if (userChips != null) {
            jsonObject = com.alibaba.fastjson.JSON.parseObject(userChips);
            Long chips = jsonObject.getLong("chips");
            if (chips != null && chips > 0) { // 还有剩余的筹码
                return chips;
            }
        }
        return 0;
    }

    private  boolean checkTableInfoIsOver(String tableId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(SNG_TABLES);
        BasicDBObject selectCondition = new BasicDBObject();
        selectCondition.put("_id", new ObjectId(tableId));
        FindIterable<Document> limitIter = collection.find(selectCondition);
        MongoCursor<Document> cursor = limitIter.iterator();
        Map<String, String> map = Maps.newHashMap();
        while (cursor.hasNext()) {
               Document doc = cursor.next();
               return doc.getInteger("isEnd") == 1;
        }
        return true;
    }

}

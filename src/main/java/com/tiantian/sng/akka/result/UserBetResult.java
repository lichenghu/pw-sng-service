package com.tiantian.sng.akka.result;

import java.io.Serializable;

/**
 *
 */
public class UserBetResult implements Serializable {
     private int sn;
     private long bet;

    public int getSn() {
        return sn;
    }

    public void setSn(int sn) {
        this.sn = sn;
    }

    public long getBet() {
        return bet;
    }

    public void setBet(long bet) {
        this.bet = bet;
    }
}

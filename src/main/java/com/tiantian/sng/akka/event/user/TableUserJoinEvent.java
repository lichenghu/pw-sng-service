package com.tiantian.sng.akka.event.user;

import com.tiantian.sng.akka.event.TableUserEvent;

import java.io.Serializable;
import java.util.List;

/**
 *
 */
public class TableUserJoinEvent extends TableUserEvent {
    private String roomId;

    private String roomName;
    private String userId;
    private String tableId;
    private int sitNum;
    private int tableIndex;
    private String avatarUrl;
    private String nickName;
    private String gender;
    private long buyIn;
    private int maxUsers;
    private List<Rule> ruleList;
    private List<Reward> rewardList;

    public TableUserJoinEvent(String roomId, String roomName, String userId, String tableId, int sitNum,
                              int tableIndex, String avatarUrl, String nickName, String gender, long buyIn, int maxUsers,
                              List<Rule> ruleList, List<Reward> rewardList) {
        super(true);
        this.roomId = roomId;
        this.roomName = roomName;
        this.userId = userId;
        this.tableId = tableId;
        this.sitNum = sitNum;
        this.tableIndex = tableIndex;
        this.avatarUrl = avatarUrl;
        this.nickName = nickName;
        this.gender = gender;
        this.buyIn = buyIn;
        this.maxUsers = maxUsers;
        this.ruleList = ruleList;
        this.rewardList = rewardList;
    }

    public static class Rule implements Serializable {
        private int level;
        private long smallBlind;
        private long bigBlind;
        private long upBlindSecs; // 升盲时间
        public Rule () {

        }
        public Rule (int level, long smallBlind, long bigBlind, long upBlindSecs) {
            this.level = level;
            this.smallBlind = smallBlind;
            this.bigBlind = bigBlind;
            this.upBlindSecs = upBlindSecs;
        }

        public int getLevel() {
            return level;
        }

        public long getSmallBlind() {
            return smallBlind;
        }

        public long getBigBlind() {
            return bigBlind;
        }

        public long getUpBlindSecs() {
            return upBlindSecs;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public void setSmallBlind(long smallBlind) {
            this.smallBlind = smallBlind;
        }

        public void setBigBlind(long bigBlind) {
            this.bigBlind = bigBlind;
        }

        public void setUpBlindSecs(long upBlindSecs) {
            this.upBlindSecs = upBlindSecs;
        }
    }

    public static class Reward implements Serializable {
        private int ranking;
        private long score;
        private String awardId;
        private String awardName;
        public Reward() {

        }
        public Reward(int ranking, long score, String awardId, String awardName) {
            this.ranking = ranking;
            this.score = score;
            this.awardId = awardId;
            this.awardName = awardName;
        }

        public int getRanking() {
            return ranking;
        }

        public void setRanking(int ranking) {
            this.ranking = ranking;
        }

        public long getScore() {
            return score;
        }

        public void setScore(long score) {
            this.score = score;
        }

        public String getAwardId() {
            return awardId;
        }

        public String getAwardName() {
            return awardName;
        }

        public void setAwardId(String awardId) {
            this.awardId = awardId;
        }

        public void setAwardName(String awardName) {
            this.awardName = awardName;
        }
    }

    public String getRoomId() {
        return roomId;
    }

    public String getUserId() {
        return userId;
    }

    public int getSitNum() {
        return sitNum;
    }

    public int getTableIndex() {
        return tableIndex;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getNickName() {
        return nickName;
    }

    public long getBuyIn() {
        return buyIn;
    }

    public List<Rule> getRuleList() {
        return ruleList;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "userJoin";
    }

    public int getMaxUsers() {
        return maxUsers;
    }

    public String getGender() {
        return gender;
    }

    public List<Reward> getRewardList() {
        return rewardList;
    }

    public String getRoomName() {
        return roomName;
    }
}

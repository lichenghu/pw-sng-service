package com.tiantian.sng.akka.event;

/**
 *
 */
public interface TableEvent extends Event {
    String tableId();
}

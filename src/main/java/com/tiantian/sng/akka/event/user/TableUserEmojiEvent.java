package com.tiantian.sng.akka.event.user;

import com.tiantian.sng.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserEmojiEvent extends TableUserEvent {
    private String tableId;
    private String userId;
    private String toUserId;
    private String emoji;

    public TableUserEmojiEvent(String tableId,  String userId, String toUserId, String emoji) {
        this.tableId = tableId;
        this.userId = userId;
        this.toUserId = toUserId;
        this.emoji = emoji;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "emoji";
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getEmoji() {
        return emoji;
    }

    public void setEmoji(String emoji) {
        this.emoji = emoji;
    }
}

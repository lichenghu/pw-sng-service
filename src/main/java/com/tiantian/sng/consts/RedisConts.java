package com.tiantian.sng.consts;

/**
 *
 */
public interface RedisConts {
   String SPINGO_ROOMS_KEY  = "sng_rooms_key:";
   String SPINGO_ROOMS_USERS_KEY  = "sng_rooms_users_key:";
   String SPINGO_ROOM_ONLINE_USERS_KEY = "sng_room_online_users_status:";
   String ROUTER_KEY = "router_key:";
   // 桌子上玩家
   String USER_SPINGO_GAMES_KEY = "user_sng_games_key:";
   String SPINGO_TABLE_STATUS_KEY = "sng_table_status:";
}

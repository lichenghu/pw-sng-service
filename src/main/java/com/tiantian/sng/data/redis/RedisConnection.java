package com.tiantian.sng.data.redis;

import com.tiantian.sng.settings.RedisConfig;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Created by zheng on 2014/11/10.
 */
public class RedisConnection {

    private static RedisConnection redisConnection;

    private JedisPool pool;

    public static RedisConnection getInstance() {
        if (redisConnection == null) {
            redisConnection = new RedisConnection();
        }
        return redisConnection;
    }

    public Jedis getConnection() {
        if (pool == null) {
            pool = new JedisPool(new JedisPoolConfig(), RedisConfig.getInstance().getHost(),
                    RedisConfig.getInstance().getPort());
        }
        return pool.getResource();
    }

    public Jedis getConnection(String host, int port) {
        if (pool == null) {
            pool = new JedisPool(new JedisPoolConfig(), host, port);
        }
        return pool.getResource();
    }

    private RedisConnection() {

    }
}

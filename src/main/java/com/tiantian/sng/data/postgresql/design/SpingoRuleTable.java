package com.tiantian.sng.data.postgresql.design;

/**
 *
 */
public class SpingoRuleTable {
    public static final String TABLE_RULE = "public.user_clubs";
    public static final String COLUMN_uc_id= "uc_id";
    public static final String COLUMN_user_id = "user_id";
    public static final String COLUMN_club_id = "club_id";
    public static final String COLUMN_score = "small_blind";
    public static final String COLUMN_UPGRADE_TICK = "upgrade_tick";
    public static final String COLUMN_PER_ADD = "per_add";
}

package com.tiantian.sng.data.postgresql.design;

/**
 *
 */
public class SngClubTable {
    public static final String TABLE_CLUB = "public.clubs";
    public static final String COLUMN_CLUB_ID = "club_id";
    public static final String COLUMN_CLUB_NAME = "club_name";
    public static final String COLUMN_CLUB_LOGO = "club_logo";
    public static final String COLUMN_CLUB_MARK = "club_mark";
}

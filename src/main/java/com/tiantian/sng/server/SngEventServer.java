package com.tiantian.sng.server;

import com.tiantian.sng.akka.ClusterClientManager;
import com.tiantian.sng.data.mongodb.MGDatabase;
import com.tiantian.sng.data.postgresql.PGDatabase;
import com.tiantian.sng.handler.SngEventHandler;
import com.tiantian.sng.settings.PGConfig;
import com.tiantian.sng.settings.SngEventConfig;
import com.tiantian.sng.thrift.event.SngEventService;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class SngEventServer {
    private static Logger LOG = LoggerFactory.getLogger(SngEventServer.class);
    private TThreadPoolServer server;
    private TThreadPoolServer.Args sArgs;

    public static void main(String[] args) {
        ClusterClientManager.init((SngEventConfig.getInstance().getPort() + 110) + "");

        SngEventServer sngEventServer = new SngEventServer();
        sngEventServer.init(args);
        sngEventServer.start();
    }

    public void init(String[] arguments) {
        LOG.info("init");
        try {
            PGDatabase.getInstance().init(
                    PGConfig.getInstance().getHost(),
                    PGConfig.getInstance().getPort(),
                    PGConfig.getInstance().getDb(),
                    PGConfig.getInstance().getUsername(),
                    PGConfig.getInstance().getPassword()
            );
            MGDatabase.getInstance().init();

            SngEventHandler handler = new SngEventHandler();
            // 启动时候清除过期数据
            handler.clearDeadTableUsers();

            TProcessor tProcessor = new SngEventService.Processor<SngEventService.Iface>(handler);
            TServerSocket tss = new TServerSocket(SngEventConfig.getInstance().getPort());
            sArgs = new TThreadPoolServer.Args(tss);
            sArgs.processor(tProcessor);
            sArgs.transportFactory(new TFastFramedTransport.Factory());
            sArgs.protocolFactory(new TCompactProtocol.Factory());

        }
        catch (Exception e) {
        }
    }

    public void start() {
        LOG.info("start");
        server = new TThreadPoolServer(sArgs);
        LOG.info("the server listen in {}", SngEventConfig.getInstance().getPort());
        server.serve();
    }

    public void stop() {
        LOG.info("stop");
        server.stop();
    }

    public void destroy() {
        LOG.info("destroy");
    }
}

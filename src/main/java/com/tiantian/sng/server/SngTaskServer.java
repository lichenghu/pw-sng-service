package com.tiantian.sng.server;

import com.tiantian.sng.akka.ClusterClientManager;
import com.tiantian.sng.data.mongodb.MGDatabase;
import com.tiantian.sng.data.postgresql.PGDatabase;
import com.tiantian.sng.handler.SngTaskHandler;
import com.tiantian.sng.settings.PGConfig;
import com.tiantian.sng.settings.SngTaskConfig;
import com.tiantian.sng.thrift.task.SngTaskService;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class SngTaskServer {
    private static Logger LOG = LoggerFactory.getLogger(SngTaskServer.class);
    private TThreadPoolServer server;
    private TThreadPoolServer.Args sArgs;

    public static void main(String[] args) {

        ClusterClientManager.init((SngTaskConfig.getInstance().getPort() + 110) + "");

        SngTaskServer sngTaskServer = new SngTaskServer();
        sngTaskServer.init(args);
        sngTaskServer.start();
    }

    public void init(String[] arguments) {
        LOG.info("init");
        try {
            PGDatabase.getInstance().init(
                    PGConfig.getInstance().getHost(),
                    PGConfig.getInstance().getPort(),
                    PGConfig.getInstance().getDb(),
                    PGConfig.getInstance().getUsername(),
                    PGConfig.getInstance().getPassword()
            );
            MGDatabase.getInstance().init();

            SngTaskHandler handler = new SngTaskHandler();
            TProcessor tProcessor = new SngTaskService.Processor<SngTaskService.Iface>(handler);
            TServerSocket tss = new TServerSocket(SngTaskConfig.getInstance().getPort());
            sArgs = new TThreadPoolServer.Args(tss);
            sArgs.processor(tProcessor);
            sArgs.transportFactory(new TFastFramedTransport.Factory());
            sArgs.protocolFactory(new TCompactProtocol.Factory());
        }
        catch (Exception e) {
        }
    }

    public void start() {
        LOG.info("start");
        server = new TThreadPoolServer(sArgs);
        LOG.info("the server listen in {}", SngTaskConfig.getInstance().getPort());
        server.serve();
    }

    public void stop() {
        LOG.info("stop");
        server.stop();
    }

    public void destroy() {
        LOG.info("destroy");
    }
}

package com.tiantian.sng.utils;

import com.alibaba.fastjson.JSON;
import com.tiantian.sng.data.redis.RedisUtil;
import com.tiantian.sng.model.GameRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class RecordUtils {
    private static Logger LOG = LoggerFactory.getLogger(RecordUtils.class);
    private static String PRE_KEY = "sng_record:";

    public static GameRecord getLastedRecord(String tableId) {
        String key = PRE_KEY + tableId;
        String result = RedisUtil.lindex(key, 0);
        if (result != null) {
            return JSON.parseObject(result, GameRecord.class);
        }
        return null;
    }

    public static void addRecord(GameRecord gameRecord, String tableId) {
        if (gameRecord == null) {
            return;
        }
        String key = PRE_KEY + tableId;
        RedisUtil.lpush(key, JSON.toJSONString(gameRecord));
        RedisUtil.expire(key, 24 * 3600);
    }

    public static void restLastedRecord(GameRecord gameRecord, String tableId) {
        if (gameRecord == null) {
            return;
        }
        String key = PRE_KEY + tableId;
        RedisUtil.lset(key,  0l, JSON.toJSONString(gameRecord));
        RedisUtil.expire(key, 24 * 3600);
    }


    public static GameRecord getRecordByIndex(String tableId, int index) {
        String key = PRE_KEY + tableId;
        String result = RedisUtil.lindex(key, index);
        if (result != null) {
            return JSON.parseObject(result, GameRecord.class);
        }
        return null;
    }
}

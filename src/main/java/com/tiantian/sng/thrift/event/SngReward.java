/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.tiantian.sng.thrift.event;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import org.apache.thrift.async.AsyncMethodCallback;
import org.apache.thrift.server.AbstractNonblockingServer.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import javax.annotation.Generated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings({"cast", "rawtypes", "serial", "unchecked"})
@Generated(value = "Autogenerated by Thrift Compiler (0.9.3)", date = "2017-01-16")
public class SngReward implements org.apache.thrift.TBase<SngReward, SngReward._Fields>, java.io.Serializable, Cloneable, Comparable<SngReward> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("SngReward");

  private static final org.apache.thrift.protocol.TField RANKING_FIELD_DESC = new org.apache.thrift.protocol.TField("ranking", org.apache.thrift.protocol.TType.I32, (short)1);
  private static final org.apache.thrift.protocol.TField PHYSICAL_REWARD_FIELD_DESC = new org.apache.thrift.protocol.TField("physicalReward", org.apache.thrift.protocol.TType.STRING, (short)2);
  private static final org.apache.thrift.protocol.TField SCORE_FIELD_DESC = new org.apache.thrift.protocol.TField("score", org.apache.thrift.protocol.TType.I64, (short)3);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new SngRewardStandardSchemeFactory());
    schemes.put(TupleScheme.class, new SngRewardTupleSchemeFactory());
  }

  public int ranking; // required
  public String physicalReward; // required
  public long score; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    RANKING((short)1, "ranking"),
    PHYSICAL_REWARD((short)2, "physicalReward"),
    SCORE((short)3, "score");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // RANKING
          return RANKING;
        case 2: // PHYSICAL_REWARD
          return PHYSICAL_REWARD;
        case 3: // SCORE
          return SCORE;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __RANKING_ISSET_ID = 0;
  private static final int __SCORE_ISSET_ID = 1;
  private byte __isset_bitfield = 0;
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.RANKING, new org.apache.thrift.meta_data.FieldMetaData("ranking", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    tmpMap.put(_Fields.PHYSICAL_REWARD, new org.apache.thrift.meta_data.FieldMetaData("physicalReward", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.SCORE, new org.apache.thrift.meta_data.FieldMetaData("score", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(SngReward.class, metaDataMap);
  }

  public SngReward() {
  }

  public SngReward(
    int ranking,
    String physicalReward,
    long score)
  {
    this();
    this.ranking = ranking;
    setRankingIsSet(true);
    this.physicalReward = physicalReward;
    this.score = score;
    setScoreIsSet(true);
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public SngReward(SngReward other) {
    __isset_bitfield = other.__isset_bitfield;
    this.ranking = other.ranking;
    if (other.isSetPhysicalReward()) {
      this.physicalReward = other.physicalReward;
    }
    this.score = other.score;
  }

  public SngReward deepCopy() {
    return new SngReward(this);
  }

  @Override
  public void clear() {
    setRankingIsSet(false);
    this.ranking = 0;
    this.physicalReward = null;
    setScoreIsSet(false);
    this.score = 0;
  }

  public int getRanking() {
    return this.ranking;
  }

  public SngReward setRanking(int ranking) {
    this.ranking = ranking;
    setRankingIsSet(true);
    return this;
  }

  public void unsetRanking() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __RANKING_ISSET_ID);
  }

  /** Returns true if field ranking is set (has been assigned a value) and false otherwise */
  public boolean isSetRanking() {
    return EncodingUtils.testBit(__isset_bitfield, __RANKING_ISSET_ID);
  }

  public void setRankingIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __RANKING_ISSET_ID, value);
  }

  public String getPhysicalReward() {
    return this.physicalReward;
  }

  public SngReward setPhysicalReward(String physicalReward) {
    this.physicalReward = physicalReward;
    return this;
  }

  public void unsetPhysicalReward() {
    this.physicalReward = null;
  }

  /** Returns true if field physicalReward is set (has been assigned a value) and false otherwise */
  public boolean isSetPhysicalReward() {
    return this.physicalReward != null;
  }

  public void setPhysicalRewardIsSet(boolean value) {
    if (!value) {
      this.physicalReward = null;
    }
  }

  public long getScore() {
    return this.score;
  }

  public SngReward setScore(long score) {
    this.score = score;
    setScoreIsSet(true);
    return this;
  }

  public void unsetScore() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __SCORE_ISSET_ID);
  }

  /** Returns true if field score is set (has been assigned a value) and false otherwise */
  public boolean isSetScore() {
    return EncodingUtils.testBit(__isset_bitfield, __SCORE_ISSET_ID);
  }

  public void setScoreIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __SCORE_ISSET_ID, value);
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case RANKING:
      if (value == null) {
        unsetRanking();
      } else {
        setRanking((Integer)value);
      }
      break;

    case PHYSICAL_REWARD:
      if (value == null) {
        unsetPhysicalReward();
      } else {
        setPhysicalReward((String)value);
      }
      break;

    case SCORE:
      if (value == null) {
        unsetScore();
      } else {
        setScore((Long)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case RANKING:
      return getRanking();

    case PHYSICAL_REWARD:
      return getPhysicalReward();

    case SCORE:
      return getScore();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case RANKING:
      return isSetRanking();
    case PHYSICAL_REWARD:
      return isSetPhysicalReward();
    case SCORE:
      return isSetScore();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof SngReward)
      return this.equals((SngReward)that);
    return false;
  }

  public boolean equals(SngReward that) {
    if (that == null)
      return false;

    boolean this_present_ranking = true;
    boolean that_present_ranking = true;
    if (this_present_ranking || that_present_ranking) {
      if (!(this_present_ranking && that_present_ranking))
        return false;
      if (this.ranking != that.ranking)
        return false;
    }

    boolean this_present_physicalReward = true && this.isSetPhysicalReward();
    boolean that_present_physicalReward = true && that.isSetPhysicalReward();
    if (this_present_physicalReward || that_present_physicalReward) {
      if (!(this_present_physicalReward && that_present_physicalReward))
        return false;
      if (!this.physicalReward.equals(that.physicalReward))
        return false;
    }

    boolean this_present_score = true;
    boolean that_present_score = true;
    if (this_present_score || that_present_score) {
      if (!(this_present_score && that_present_score))
        return false;
      if (this.score != that.score)
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    List<Object> list = new ArrayList<Object>();

    boolean present_ranking = true;
    list.add(present_ranking);
    if (present_ranking)
      list.add(ranking);

    boolean present_physicalReward = true && (isSetPhysicalReward());
    list.add(present_physicalReward);
    if (present_physicalReward)
      list.add(physicalReward);

    boolean present_score = true;
    list.add(present_score);
    if (present_score)
      list.add(score);

    return list.hashCode();
  }

  @Override
  public int compareTo(SngReward other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = Boolean.valueOf(isSetRanking()).compareTo(other.isSetRanking());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetRanking()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.ranking, other.ranking);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetPhysicalReward()).compareTo(other.isSetPhysicalReward());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetPhysicalReward()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.physicalReward, other.physicalReward);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetScore()).compareTo(other.isSetScore());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetScore()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.score, other.score);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("SngReward(");
    boolean first = true;

    sb.append("ranking:");
    sb.append(this.ranking);
    first = false;
    if (!first) sb.append(", ");
    sb.append("physicalReward:");
    if (this.physicalReward == null) {
      sb.append("null");
    } else {
      sb.append(this.physicalReward);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("score:");
    sb.append(this.score);
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class SngRewardStandardSchemeFactory implements SchemeFactory {
    public SngRewardStandardScheme getScheme() {
      return new SngRewardStandardScheme();
    }
  }

  private static class SngRewardStandardScheme extends StandardScheme<SngReward> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, SngReward struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // RANKING
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.ranking = iprot.readI32();
              struct.setRankingIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // PHYSICAL_REWARD
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.physicalReward = iprot.readString();
              struct.setPhysicalRewardIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // SCORE
            if (schemeField.type == org.apache.thrift.protocol.TType.I64) {
              struct.score = iprot.readI64();
              struct.setScoreIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, SngReward struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(RANKING_FIELD_DESC);
      oprot.writeI32(struct.ranking);
      oprot.writeFieldEnd();
      if (struct.physicalReward != null) {
        oprot.writeFieldBegin(PHYSICAL_REWARD_FIELD_DESC);
        oprot.writeString(struct.physicalReward);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldBegin(SCORE_FIELD_DESC);
      oprot.writeI64(struct.score);
      oprot.writeFieldEnd();
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class SngRewardTupleSchemeFactory implements SchemeFactory {
    public SngRewardTupleScheme getScheme() {
      return new SngRewardTupleScheme();
    }
  }

  private static class SngRewardTupleScheme extends TupleScheme<SngReward> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, SngReward struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      BitSet optionals = new BitSet();
      if (struct.isSetRanking()) {
        optionals.set(0);
      }
      if (struct.isSetPhysicalReward()) {
        optionals.set(1);
      }
      if (struct.isSetScore()) {
        optionals.set(2);
      }
      oprot.writeBitSet(optionals, 3);
      if (struct.isSetRanking()) {
        oprot.writeI32(struct.ranking);
      }
      if (struct.isSetPhysicalReward()) {
        oprot.writeString(struct.physicalReward);
      }
      if (struct.isSetScore()) {
        oprot.writeI64(struct.score);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, SngReward struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      BitSet incoming = iprot.readBitSet(3);
      if (incoming.get(0)) {
        struct.ranking = iprot.readI32();
        struct.setRankingIsSet(true);
      }
      if (incoming.get(1)) {
        struct.physicalReward = iprot.readString();
        struct.setPhysicalRewardIsSet(true);
      }
      if (incoming.get(2)) {
        struct.score = iprot.readI64();
        struct.setScoreIsSet(true);
      }
    }
  }

}


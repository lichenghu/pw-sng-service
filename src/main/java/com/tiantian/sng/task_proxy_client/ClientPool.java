package com.tiantian.sng.task_proxy_client;

import com.tiantian.sng.settings.SngTaskConfig;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.thrift.transport.TSocket;

/**
 *
 */
public class ClientPool {
    private GenericObjectPool<TSocket> socketPool;

    public TSocket borrowObject() throws Exception {
        return socketPool.borrowObject();
    }

    public void returnObject(TSocket socket) {
        socketPool.returnObject(socket);
    }

    private ClientPool() {
        // 初始化对象池配置
        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnCreate(true);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setTimeBetweenEvictionRunsMillis(60000);
        // 初始化对象池
        socketPool = new GenericObjectPool<>(new ClientFactory(
                SngTaskConfig.getInstance().getHost(),
                SngTaskConfig.getInstance().getPort()
        ), poolConfig);
    }

    private static class ClientPoolHolder {
        private final static ClientPool instance = new ClientPool();
    }

    public static ClientPool getInstance() {
        return ClientPoolHolder.instance;
    }
}

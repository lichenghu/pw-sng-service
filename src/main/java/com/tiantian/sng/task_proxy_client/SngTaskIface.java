package com.tiantian.sng.task_proxy_client;

import com.tiantian.sng.thrift.task.SngTaskService;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TSocket;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;

/**
 *
 */
public class SngTaskIface {
    private static SngTaskService.Iface clientProxy;

    private static class SngTaskIfaceHolder {
        private final static SngTaskIface instance = new SngTaskIface();
    }
    private SngTaskIface() {
        ClassLoader classLoader = SngTaskService.Iface.class.getClassLoader();
        clientProxy = (SngTaskService.Iface) Proxy.newProxyInstance(classLoader,
                new Class[]{SngTaskService.Iface.class},
                (proxy, method, args) -> {
                    TSocket ttSocket = ClientPool.getInstance().borrowObject();
                    try {
                        TFastFramedTransport fastTransport = new TFastFramedTransport(ttSocket);
                        TCompactProtocol protocol = new TCompactProtocol(fastTransport);
                        SngTaskService.Client client = new SngTaskService.Client(protocol);
                        //设置成可以访问
                        method.setAccessible(true);
                        return method.invoke(client, args);
                    } catch (InvocationTargetException | IllegalAccessException | IllegalArgumentException e) {
                        if (e instanceof InvocationTargetException) {
                            Throwable throwable = ((InvocationTargetException) e).getTargetException();
                            throwable.printStackTrace();
                            throw throwable;
                        }
                        e.printStackTrace();
                        throw e;
                    } finally {
                        ClientPool.getInstance().returnObject(ttSocket);
                    }
                });
    }

    public static SngTaskIface instance() {
        return SngTaskIfaceHolder.instance;
    }

    public SngTaskService.Iface iface() {
        return clientProxy;
    }
}

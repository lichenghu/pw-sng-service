package com.tiantian.sng.entity;

/**
 *
 */
public class SngRuleEntity {
    private int level;
    private long smallBlind;
    private long bigBlind;
    private long upBlindSecs; // 升盲时间

    public SngRuleEntity(int level, long smallBlind, long bigBlind, long upBlindSecs) {
        this.level = level;
        this.smallBlind = smallBlind;
        this.bigBlind = bigBlind;
        this.upBlindSecs = upBlindSecs;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public long getSmallBlind() {
        return smallBlind;
    }

    public void setSmallBlind(long smallBlind) {
        this.smallBlind = smallBlind;
    }

    public long getBigBlind() {
        return bigBlind;
    }

    public void setBigBlind(long bigBlind) {
        this.bigBlind = bigBlind;
    }

    public long getUpBlindSecs() {
        return upBlindSecs;
    }

    public void setUpBlindSecs(long upBlindSecs) {
        this.upBlindSecs = upBlindSecs;
    }
}

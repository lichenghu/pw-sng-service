package com.tiantian.sng.entity;

/**
 * sng奖励信息
 */
public class SngRewardEntity {
    private String roomId;
    private int ranking;
    private String physicalReward;
    private String awardId;
    private long score;

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public String getPhysicalReward() {
        return physicalReward;
    }

    public void setPhysicalReward(String physicalReward) {
        this.physicalReward = physicalReward;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public String getAwardId() {
        return awardId;
    }

    public void setAwardId(String awardId) {
        this.awardId = awardId;
    }
}

package com.tiantian.sng.entity;

import java.util.List;

/**
 *
 */
public class SngRoomEntity {
    private String roomId;
    private String roomName;
    private String roomDesc;
    private String clubId;
    private int isEnd;
    private int userCnt;
    private long fee; // 报名费积分
    private int maxTableNums; // 最多桌数 <= 0 表示不限制
    private long buyIn;
    private List<SngRuleEntity> rules; //规则
    private List<SngRewardEntity> sngRewards;

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomDesc() {
        return roomDesc;
    }

    public void setRoomDesc(String roomDesc) {
        this.roomDesc = roomDesc;
    }

    public long getFee() {
        return fee;
    }

    public void setFee(long fee) {
        this.fee = fee;
    }

    public List<SngRuleEntity> getRules() {
        return rules;
    }

    public void setRules(List<SngRuleEntity> rules) {
        this.rules = rules;
    }

    public List<SngRewardEntity> getSngRewards() {
        return sngRewards;
    }

    public void setSngRewards(List<SngRewardEntity> sngRewards) {
        this.sngRewards = sngRewards;
    }

    public int getMaxTableNums() {
        return maxTableNums;
    }

    public void setMaxTableNums(int maxTableNums) {
        this.maxTableNums = maxTableNums;
    }

    public String getClubId() {
        return clubId;
    }

    public void setClubId(String clubId) {
        this.clubId = clubId;
    }

    public int getIsEnd() {
        return isEnd;
    }

    public void setIsEnd(int isEnd) {
        this.isEnd = isEnd;
    }

    public int getUserCnt() {
        return userCnt;
    }

    public void setUserCnt(int userCnt) {
        this.userCnt = userCnt;
    }

    public long getBuyIn() {
        return buyIn;
    }

    public void setBuyIn(long buyIn) {
        this.buyIn = buyIn;
    }
}

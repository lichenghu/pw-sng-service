package com.tiantian.sng.settings;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.bpsm.edn.parser.Parseable;
import us.bpsm.edn.parser.Parser;
import us.bpsm.edn.parser.Parsers;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Map;

import static us.bpsm.edn.Keyword.newKeyword;
import static us.bpsm.edn.parser.Parsers.defaultConfiguration;

/**
 * Created by jeffma on 15/10/27.
 */
public class PGConfig {
    private Logger LOG= LoggerFactory.getLogger(PGConfig.class);
    private String configFile = "config/postgresql.edn";
    private Map<?, ?> appConf;
    private int port;
    private String host;
    private String env;
    private String db;
    private String username;
    private String password;

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHost() {
        return host;
    }
    public void setHost(String host) {
        this.host = host;
    }
    public int getPort() {
        return port;
    }
    public void setPort(int port) {
        this.port = port;
    }

    private PGConfig(){
        env = System.getenv("NOMAD_ENV");
        if(null == env || "" == env){
            env = "development";
        }
        InputStream inputConfig = this.getClass().getClassLoader().getResourceAsStream(configFile);
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputConfig, writer, "utf8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String configString = writer.toString();
        LOG.info("==> config {}",configString);
        LOG.info("env is {}",env);
        Parseable pbr = Parsers.newParseable(configString);
        Parser p = Parsers.newParser(defaultConfiguration());
        appConf = (Map<?, ?>) p.nextValue(pbr);
        Map<?, ?> envs = (Map<?, ?>)appConf.get(newKeyword("nomad","environments"));
        Map<?, ?> appEnv = (Map<?, ?>)envs.get(env);
        this.port = Integer.parseInt(String.valueOf(appEnv.get(newKeyword("port"))));
        this.host = String.valueOf(appEnv.get(newKeyword("host")));
        this.db = String.valueOf(appEnv.get(newKeyword("db")));
        this.username = String.valueOf(appEnv.get(newKeyword("username")));
        this.password = String.valueOf(appEnv.get(newKeyword("password")));
        LOG.info("port is {}", this.port);
        LOG.info("host is {}", this.host);
        LOG.info("db is {}", this.db);
        LOG.info("password is {}", this.password);
        LOG.info("username is {}", this.username);
    }
    private static class PGConfigHolder{
        private final static PGConfig instance = new PGConfig();
    }
    public static PGConfig getInstance(){
        return PGConfigHolder.instance;
    }
}

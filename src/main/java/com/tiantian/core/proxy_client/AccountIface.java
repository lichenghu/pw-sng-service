package com.tiantian.core.proxy_client;

import com.tiantian.core.settings.PassportConfig;
import com.tiantian.core.thrift.account.AccountService;
import com.tiantian.framework.thrift.client.*;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.protocol.TProtocol;

/**
 *
 */
public class AccountIface extends IFace<AccountService.Iface> {
    private static class CoreIFaceHolder {
        private final static AccountIface instance = new AccountIface();
    }
    public static AccountIface instance() {
        return CoreIFaceHolder.instance;
    }


    @Override
    public TServiceClient createClient(TProtocol tProtocol) {
        return new AccountService.Client(tProtocol);
    }

    @Override
    public com.tiantian.framework.thrift.client.ClientPool createPool() {
        return new com.tiantian.framework.thrift.client.ClientPool(PassportConfig.getInstance().getHost(),
                PassportConfig.getInstance().getPort());
    }

    @Override
    protected Class<AccountService.Iface> faceClass() {
        return AccountService.Iface.class;
    }
}

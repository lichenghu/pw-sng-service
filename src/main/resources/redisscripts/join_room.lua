local keyLen = #KEYS
if keyLen ~= 4 then
    return {}
end
local roomId = KEYS[1]
local userId = KEYS[2]
local roomIdKeyPre = KEYS[3]
local waitRoomKey = KEYS[4]
local roomIdKey = roomIdKeyPre .. roomId
local allUsers = redis.pcall("LRANGE", roomIdKey, 0, -1)
local hasUser = false -- 队列中是否已经存在该玩家ID
for i, v in pairs(allUsers) do
    if v == userId then
       hasUser = true
       break
    end
end

if not hasUser then
   redis.pcall("rpush", roomIdKey, userId)
   redis.pcall("set", waitRoomKey .. userId, roomId)
end

local len = redis.pcall("LLEN", roomIdKey)
local userIds = {}
if len >= 3 then
  for i = 1 , 3 do
      local userId = redis.pcall("LPOP", roomIdKey)
      table.insert(userIds, userId)
  end
  return userIds
else
  return {}
end
local keyLen = #KEYS
if keyLen ~= 4 then
    return 0
end
local roomId = KEYS[1]
local userId = KEYS[2]
local roomIdKeyPre = KEYS[3]
local waitRoomKeyPre = KEYS[4]
local roomIdKey = roomIdKeyPre .. roomId
local allUsers = redis.pcall("LRANGE", roomIdKey, 0, -1)
local isOk = 0
for _, v in pairs(allUsers) do  -- 判断队列中是否已经存在该玩家ID
    if v == userId then
        isOk = 1
        redis.pcall("lrem", roomIdKey, 1, userId)
        redis.pcall("del", waitRoomKeyPre .. userId)
        break
    end
end

return isOk

